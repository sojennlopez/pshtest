import React from 'react';
import logo from './logo.svg';
import './App.css';
import ReactDOM from 'react-dom';
function getJugadores() {
  fetch("/jugadores/")
      
      .then((response) => {
        return response.json()
      })
      .then((jugadores) => {
        console.log(jugadores)
      var listaResultados = jugadores.map((resultado, index )=> {
       return (
        <tr key={index}>
          <td><center>{resultado.idJugador}</center></td>
          <td><center>{resultado.IdEstadistica}</center></td>
          <td><center>{resultado.nombre}</center></td>
          <td><center><img src={resultado.imagen}></img></center></td>
          <td><center>{resultado.fechaDeCreacion}</center></td>
         <td><center>{resultado.puntaje}</center></td> 
        </tr>
      )
    });
    
    ReactDOM.render(listaResultados, document.getElementById('idBody'));
    console.log(listaResultados);


      }) 
}
function archivos (){

  fetch("/jugadores/")
      
      .then((response) => {
        return response.json()
      })
      .then((jugadores) => {
        console.log(jugadores)
          
    let csvContent = "data:text/csv;charset=utf-8,";
    csvContent += "Numero de jugador;Numero de Estadistica;Nombre del jugador;Imagen;Fecha de creacion;Puntuacion"+ "\r\n";
    jugadores.forEach(function(jugadoresArray) {
      var row = ""; 
       ["idJugador","IdEstadistica","nombre", "imagen", "fechaDeCreacion", "puntaje" ].forEach(function(columna){
      var dato = jugadoresArray[columna];
        row +=dato.toString()+ ";";
       })
    
        csvContent += row + "\r\n";
    });    
      var encodedUri = encodeURI(csvContent);
  var link = document.createElement("a");
  link.setAttribute("href", encodedUri);
  link.setAttribute("download", "my_data.csv");
  document.body.appendChild(link); // Required for FF

  link.click(); // This will download the data file named "my_data.csv".
      }) 
}

function App() {
     getJugadores()
     setInterval(function() {
           getJugadores()}, 10000);
   return (
    <div id="div1">
        <button onClick={archivos} type="button" id="btn1">Descargar Reporte</button>
        <table className="table table-striped table-dark">
            <thead> 
              <tr>
                <th><center>Numero de jugador</center></th>
                <th><center>Numero de Estadistica</center></th>
                <th><center>Nombre del jugador</center></th>
                <th><center>Imagen</center></th>
                <th><center>Fecha de creacion</center></th>
                <th><center>Puntuacion</center></th>
              </tr>
            </thead>
            <tbody id="idBody">
            </tbody>
        </table>
    </div>
  );

}


export default App;
