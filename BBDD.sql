CREATE DATABASE `pshtest` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pshtest`;
CREATE TABLE `jugadores` (
  `idJugador` int(11) NOT NULL AUTO_INCREMENT,
  `IdEstadistica` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `fechaDeCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `puntaje` int(11) NOT NULL,
  PRIMARY KEY (`idJugador`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

